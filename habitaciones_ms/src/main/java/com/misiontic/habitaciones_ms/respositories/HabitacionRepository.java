package com.misiontic.habitaciones_ms.respositories;

import com.misiontic.habitaciones_ms.models.Habitacion;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface HabitacionRepository extends MongoRepository <Habitacion, Long>{}
