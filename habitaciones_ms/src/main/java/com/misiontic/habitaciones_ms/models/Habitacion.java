package com.misiontic.habitaciones_ms.models;

import org.springframework.data.annotation.Id;


public class Habitacion {
    @Id
    private Long id;
    private String numeroHabitacion;
    private String cantidadCamas;
    private String tipoCama;
    private String acomodacion;
    private Long valorporNoche;


    /*public Habitacion(Long id, String numeroHabitacion, String cantidadCamas, String tipoCama, String acomodacion, Long valorporNoche){
        this.id = id;
        this.numeroHabitacion = numeroHabitacion;
        this.cantidadCamas = cantidadCamas;
        this.tipoCama = tipoCama;
        this.acomodacion = acomodacion;
        this.valorporNoche = valorporNoche;


    }

     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroHabitacion() {
        return numeroHabitacion;
    }

    public void setNumeroHabitacion(String numeroHabitacion) {
        this.numeroHabitacion = numeroHabitacion;
    }

    public String getCantidadCamas() {
        return cantidadCamas;
    }

    public void setCantidadCamas(String cantidadCamas) {
        this.cantidadCamas = cantidadCamas;
    }

    public String getTipoCama() {
        return tipoCama;
    }

    public void setTipoCama(String tipoCama) {
        this.tipoCama = tipoCama;
    }

    public String getAcomodacion() {
        return acomodacion;
    }

    public void setAcomodacion(String acomodacion) {
        this.acomodacion = acomodacion;
    }

    public Long getValorporNoche() {
        return valorporNoche;
    }

    public void setValorporNoche(Long valorporNoche) {
        this.valorporNoche = valorporNoche;
    }
}

