package com.misiontic.habitaciones_ms.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class HabitacionNotFoundException extends RuntimeException{

    public HabitacionNotFoundException(String message){
        super(message);
    }
}
