package com.misiontic.habitaciones_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HabitacionesMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HabitacionesMsApplication.class, args);
	}

}
