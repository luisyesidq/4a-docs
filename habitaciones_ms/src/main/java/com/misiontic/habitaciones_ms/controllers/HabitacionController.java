package com.misiontic.habitaciones_ms.controllers;

import  com.misiontic.habitaciones_ms.exceptions.HabitacionNotFoundException;
import  com.misiontic.habitaciones_ms.models.Habitacion;
import  com.misiontic.habitaciones_ms.respositories.HabitacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.web.bind.annotation.*;
import  org.springframework.http.HttpStatus;
import  org.springframework.http.ResponseEntity;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping

public class HabitacionController {

    @Autowired
    private  final HabitacionRepository habitacionRepository;

    public HabitacionController(HabitacionRepository habitacionRepository){
        this.habitacionRepository = habitacionRepository;
    }

    @GetMapping("/habitaciones")
    public List<Habitacion> getAllHabitaciones(){
        return  habitacionRepository.findAll();
    }

    @PostMapping("/habitaciones")
    public Habitacion createHabitacion(@RequestBody Habitacion habitacion){
        return  habitacionRepository.save(habitacion);
    }

    @GetMapping("/habitacion/{id}")
    public ResponseEntity<Habitacion> getHabitacionById(@PathVariable Long id){
        Habitacion habitacion = habitacionRepository.findById(id).orElseThrow(() -> new HabitacionNotFoundException("Habitacion no disponible:" + id));
        return  ResponseEntity.ok(habitacion);
    }

    @PutMapping("/habitacion/{id}")
    public ResponseEntity<Habitacion> updateHabitacion(@PathVariable Long id, @RequestBody Habitacion habitacionDetails){
        Habitacion updateHabitacion = habitacionRepository.findById(id).orElseThrow(() -> new HabitacionNotFoundException("La habitacion se actualizo correctamente"));

        updateHabitacion.setNumeroHabitacion(habitacionDetails.getNumeroHabitacion());
        updateHabitacion.setCantidadCamas(habitacionDetails.getCantidadCamas());
        updateHabitacion.setTipoCama(habitacionDetails.getTipoCama());
        updateHabitacion.setAcomodacion(habitacionDetails.getAcomodacion());
        updateHabitacion.setValorporNoche(habitacionDetails.getValorporNoche());

        habitacionRepository.save(updateHabitacion);

        return ResponseEntity.ok(updateHabitacion);
    }

    @DeleteMapping("/habitacion/{id}")
    public ResponseEntity<HttpStatus> deleteHabitacion(@PathVariable Long id){
        Habitacion habitacion = habitacionRepository.findById(id).orElseThrow(() -> new HabitacionNotFoundException("La habitacion se elimino correctamente:" + id));

        habitacionRepository.delete(habitacion);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}



