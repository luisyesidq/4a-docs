package com.reservas.reserva.dao.api;

import com.reservas.reserva.model.Reserva;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReservaRepositorio extends MongoRepository<Reserva, Long> {

}
