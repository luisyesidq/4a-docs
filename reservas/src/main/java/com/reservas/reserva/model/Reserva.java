package com.reservas.reserva.model;

import org.springframework.data.annotation.Id;

public class Reserva {
    @Id
    private Long id;
    private String numeroidentificacion_huesped;
    private String numeroHabitacion;
    private String fechaEntrada;
    private String fechaSalida;
    private String estado;

    public Reserva() {
    }

    public Reserva(Long id, String numeroidentificacion_huesped, String numeroHabitacion, String fechaEntrada, String fechaSalida, String estado) {
        this.id = id;
        this.numeroidentificacion_huesped = numeroidentificacion_huesped;
        this.numeroHabitacion = numeroHabitacion;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroidentificacion_huesped() {
        return numeroidentificacion_huesped;
    }

    public void setNumeroidentificacion_huesped(String numeroidentificacion_huesped) {
        this.numeroidentificacion_huesped = numeroidentificacion_huesped;
    }

    public String getNumeroHabitacion() {
        return numeroHabitacion;
    }

    public void setNumeroHabitacion(String numeroHabitacion) {
        this.numeroHabitacion = numeroHabitacion;
    }

    public String getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(String fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
