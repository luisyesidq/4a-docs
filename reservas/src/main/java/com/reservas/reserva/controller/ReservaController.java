package com.reservas.reserva.controller;

import com.reservas.reserva.model.Reserva;
import com.reservas.reserva.service.api.ReservaServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/reservas/api/ms3")
@CrossOrigin("*")
public class ReservaController {

    @Autowired
    private ReservaServiceApi reservaServiceApi;


    @GetMapping(value = "/all")
    public List<Reserva> getAll()
    {
        return reservaServiceApi.getAll();
    }

    @GetMapping(value = "/find/{id}")
    public Reserva find(@PathVariable Long id)
    {
        return reservaServiceApi.get(id);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Reserva> save(@RequestBody Reserva huesped)
    {
        Reserva obj = reservaServiceApi.save(huesped);
        return new ResponseEntity<Reserva>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Reserva> delete(@PathVariable Long id)
    {
        Reserva huesped = reservaServiceApi.get(id);
        if(huesped!=null)
        {
            reservaServiceApi.delete(id);
        }
        else
        {
            return new ResponseEntity<Reserva>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Reserva>(HttpStatus.OK);
    }
}
