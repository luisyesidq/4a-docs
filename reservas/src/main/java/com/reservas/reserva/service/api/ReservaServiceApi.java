package com.reservas.reserva.service.api;

import com.reservas.reserva.commons.GenericServiceApi;
import com.reservas.reserva.model.Reserva;

public interface ReservaServiceApi extends GenericServiceApi<Reserva, Long> {
}
