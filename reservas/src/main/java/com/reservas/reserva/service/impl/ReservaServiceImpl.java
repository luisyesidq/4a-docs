package com.reservas.reserva.service.impl;

import com.reservas.reserva.commons.GenericServiceImpl;
import com.reservas.reserva.dao.api.ReservaRepositorio;
import com.reservas.reserva.model.Reserva;
import com.reservas.reserva.service.api.ReservaServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class ReservaServiceImpl extends GenericServiceImpl<Reserva, Long> implements ReservaServiceApi {

    @Autowired
    private ReservaRepositorio huespedRepository;

    @Override
    public CrudRepository<Reserva, Long> getDao() {
        return huespedRepository;
    }
}
