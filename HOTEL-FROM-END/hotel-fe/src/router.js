import gql from "graphql-tag";
import { createRouter, createWebHistory } from "vue-router";
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core' 
 
//importamos los componentes a usar
import LogIn from './components/LogIn.vue' 
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import Galeria from './components/Galeria.vue'
import RegistroHuespedes from './components/RegistroHuespedes.vue'
import BuscarHuesped from './components/BuscarHuesped.vue'
import RegistrarHabitaciones from './components/RegistrarHabitaciones.vue'
import BuscarHabitacion from './components/BuscarHabitacion.vue'
import CheckIn from './components/CheckIn.vue'



/**
 * ‘/’: La cual corresponde al componente principal APP.  
 ‘/user/LogIn’: La cual corresponde al componente LogIn.  
 ‘/user/signUp’: La cual corresponde al componente SignUp. 
 */
const routes = [
    { 
        path: '/user/logIn', 
        name: "logIn", 
        component: LogIn, 
        meta: { requiresAuth: false }
    }, 
    { 
        path: '/user/signUp', 
        name: "signUp", 
        component: SignUp,
        meta: { requiresAuth: false }
    },
    { 
        path: '/user/account', 
        name: "account", 
        component: Account,
        meta: { requiresAuth: false } 
    },
    { 
        path: '/user/home', 
        name: "home", 
        component: Home,
        meta: { requiresAuth: true }
    }
    ,
    { 
        path: '/user/galeria', 
        name: "galeria", 
        component: Galeria,
        meta: { requiresAuth: true } 
    },
    { 
        path: '/user/registroHuespedes', 
        name: "registroHuespedes", 
        component: RegistroHuespedes,
        meta: { requiresAuth: true }
    }
    ,
    { 
        path: '/user/buscarHuesped', 
        name: "buscarHuesped", 
        component: BuscarHuesped,
        meta: { requiresAuth: true } 
    }
    ,
    { 
        path: '/user/registrarHabitaciones', 
        name: "registrarHabitaciones", 
        component: RegistrarHabitaciones,
        meta: { requiresAuth: true } 
    }
    ,
    { 
        path: '/user/buscarHabitacion', 
        name: "buscarHabitacion", 
        component: BuscarHabitacion,
        meta: { requiresAuth: true } 
    }
    ,
    { 
        path: '/user/checkIn', 
        name: "checkIn", 
        component: CheckIn,
        meta: { requiresAuth: true } 
    }
   
  
   
];
const router = createRouter({
    history: createWebHistory(),
    routes,
    });
 
const apolloClient = new ApolloClient({
    link: createHttpLink({ uri: 'https://hotel-api-p45.herokuapp.com/' }),
    cache: new InMemoryCache()
})
    async function isAuth() {
    if (localStorage.getItem("token_access") === null ||
localStorage.getItem("token_refresh") === null) {
        return false;
    }
    try {
        var result = await apolloClient.mutate({
            mutation: gql `
                mutation ($refresh: String!) {
                    refreshToken(refresh: $refresh) {
                    access
                    }
                }
            `,
            variables: {
                refresh: localStorage.getItem("token_refresh"),
            },
        })
        localStorage.setItem("token_access", result.data.refreshToken.access);
        return true;
    } catch {
        localStorage.clear();
        alert("Su sesión expiró, por favor vuelva a iniciar sesión");
        return false;
        }
    }
router.beforeEach(async(to, from) => {
    var is_auth = await isAuth();
    if (is_auth == to.meta.requiresAuth) return true
    if (is_auth) return { name: "home" };
    return { name: "logIn" };
})
export default router;
