<template>

 <div class="">
    <div class="">
      <div class="">
          <h2>Huesped</h2>
          
      <form v-on:submit.prevent="processGetHuesped">
          <input 
            type="text"
            class="form-control"
            v-model="elementP.documento"
            placeholder="Numero Documento"
          />
          <button type="submit" class="btn btn-primary">Buscar Huesped</button>
        </form>
        
      </div>
      <br>
      <div class="">
        
        <form v-on:submit.prevent="processCheckIn">
          <input
            type="number"
            class="form-control"
            v-model="onlyOneP.documento"
            placeholder="Documento"
          />
          <input
            type="text"
            class="form-control"
            v-model="onlyOneP.nombre"
            placeholder="Nombre"
          />
          
          <input
            type="text"
            class="form-control"
            v-model="onlyOneP.apellido"
            placeholder="Apellido"
          />
             <input
            type="number"
            class="form-control"
            v-model="onlyOneP.id"
            placeholder="id"
          />
         
            <br />
         
           <h2>Habitacion</h2>
            <form v-on:submit.prevent="processGetHabitacion">
                <input 
                    type="text"
                    class="form-control"
                    v-model="elementP.numero_habitacion"
                    placeholder="Numero Habitacion"
                />
                <button type="submit" class="btn btn-primary">Buscar Habitacion</button>
                </form>
                <input
                    type="number"
                    class="form-control"
                    v-model="onlyOneP.id"
                    placeholder="ID"
                />
                <input
                    type="number"
                    class="form-control"
                    v-model="onlyOneP.numero_camas"
                    placeholder="Numero de Camas"
                />
                
                <input
                    type="text"
                    class="form-control"
                    v-model="onlyOneP.descripcion"
                    placeholder="Descripcion"
                />
                <input
                    type="number"
                    class="form-control"
                    v-model="onlyOneP.precio"
                    placeholder="Precio"
                />
                <input
                    type="text"
                    class="form-control"
                    v-model="onlyOneP.observaciones"
                    placeholder="Observaciones"
                />
                
                <br />
                <input
                    type="text"
                    class="form-control"
                    v-model="onlyOneP.observaciones"
                    placeholder="Observaciones"
                />
                
              
               
           
          <button type="submit" class="btn btn-primary">CHECK-IN</button>
        </form>
      </div>
    </div>

      
  </div>
</template>
<script>
import axios from "axios";
export default {
  name: "CheckIn",
  data: function () {
    return {
      username: localStorage.getItem("username") || "none",
      products: [],
      elementP: {
        id: "",
        nombre: "",
        apellido: "",
      },
      onlyOneP: {
        nombre: "",
        apellido: "",
      },
    };
  }, 
  methods: {
    processData: function () {
      axios
        .get("https://gran-hotel-backend.herokuapp.com/huespedes/", {
          headers: {},
        })
        .then((result) => {
          console.log(result.data);
          this.products = result.data;
        })
        .catch((error) => {
          alert(error);
        });
     axios
        .get("https://gran-hotel-backend.herokuapp.com/habitaciones/", {
          headers: {},
        })
        .then((result) => {
          console.log(result.data);
          this.products = result.data;
        })
        .catch((error) => {
          alert(error);
        });
    },
    
    processGetHuesped: function () {
      axios
        .get(
          `https://gran-hotel-backend.herokuapp.com/huespedes/${this.elementP.documento}`,
          this.elementP,
          {
            headers: {},
          }
        )
        .then((result) => {
          this.onlyOneP.id = result.data.id;
          this.onlyOneP.documento = result.data.documento;
          this.onlyOneP.nombre = result.data.nombre;
          this.onlyOneP.apellido = result.data.apellido;
          
          
          
        })
        .catch((error) => {
          alert(error);
        });
    },
      
    
    processGetHabitacion: function () {
      axios
        .get(
          `https://gran-hotel-backend.herokuapp.com/habitaciones/${this.elementP.numero_habitacion}`,
          this.elementP,
          {
            headers: {},
          }
        )
        .then((result) => {
          this.onlyOneP.id = result.data.id;
          this.onlyOneP.numero_habitacion = result.data.numero_habitacion;
          this.onlyOneP.numero_camas = result.data.numero_camas;
          this.onlyOneP.descripcion = result.data.descripcion;
          this.onlyOneP.precio = result.data.precio;
          this.onlyOneP.observaciones = result.data.observaciones;
          
        })
        .catch((error) => {
          alert(error);
        });
    },
     processCheckIn: function(){ 
            axios.post( 
                "https://gran-hotel-backend.herokuapp.com/checkIn/",  
                this.onlyOneP.id, 
                this.onlyOneP.id,   
                {
            headers: {},
          }
        )
        .then((result) => {
          alert(result.status + "Check-in Exitoso...");
        })
        .catch((error) => {
          alert(error);
        }); 
        } 
 
 
  },
  created: function () {
    this.processData();
  },
  
};

</script>