//Se llama al typedef (esquema) de cada submodulo
const userAdminTypeDefs      = require('./userAdmin_type_defs');
const habitacionesTypeDefs   = require('./habitaciones_type_defs');
const huespedesTypeDefs      = require('./huespedes_type_defs');
const reservasTypeDefs      = require('./reservas_type_defs');
//Se unen
const schemasArrays = [userAdminTypeDefs, habitacionesTypeDefs, huespedesTypeDefs, reservasTypeDefs];
//Se exportan
module.exports = schemasArrays;