const { gql } = require('apollo-server');

const huespedesTypeDefs = gql `
    type Huesped {
        id: Int
        numeroidentificacion: String!
        nombres: String!
        apellidos: String!
        paisResidencia: String!
        direccion: String!
        telefono: String!
        correoElectronico: String!
    }

    input HuespedInput {
        id: Int!
        numeroidentificacion: String!
        nombres: String!
        apellidos: String!
        paisResidencia: String!
        direccion: String!
        telefono: String!
        correoElectronico:  String!
    }

    type Query {
        huespedById(huespedesId: Int!): Huesped
    }

    type Mutation{
        createHuesped(huesped: HuespedInput!): Huesped
        updateHuesped(huesped: HuespedInput!): Huesped
        deleteHuespedById(id: Int!): Huesped
    }
`;  

module.exports = huespedesTypeDefs;