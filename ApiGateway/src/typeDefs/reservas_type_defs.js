const { gql } = require('apollo-server');

const reservasTypeDefs = gql `
    type Reserva {
        id: Int
        numeroidentificacion_huesped: String!
        numeroHabitacion: String!
        fechaEntrada: String!
        fechaSalida: String!
        estado: String!
    }

    input ReservaInput {
        id: Int!
        numeroidentificacion_huesped: String!
        numeroHabitacion: String!
        fechaEntrada: String!
        fechaSalida: String!
        estado: String!
    }

    type Query {
        reservaById(reservaId: Int!): Reserva
    }

    type Mutation  {
        createReserva(reserva: ReservaInput!): Reserva
        updateReserva(reserva: ReservaInput!): Reserva
        deleteReservaById(id: Int!): Reserva
    }
`;  

module.exports = reservasTypeDefs;