const { gql } = require('apollo-server');

const userAdminTypeDefs = gql `
    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    input CredentialsInput {
        username: String!
        password: String!
    }

    type UserAdmin {
        id: Int
        cedula: Int!
        username: String!
        password: String!
        name: String!
        last_name: String!
        cargo: String!
        email: String!
        adress: String!
        phoneNumber: Int!
    }

    input SignUpInput {
        cedula: Int!
        username: String!
        password: String!
        name: String!
        last_name: String!
        cargo: String!
        email: String!
        adress: String!
        phoneNumber: Int!
    }

    type UserDetail {
        id: Int!
        cedula: Int!
        username: String!
        password: String!
        name: String!
        last_name: String!
        cargo: String!
        email: String!
        adress: String!
        phoneNumber: Int!
    }

    input UserInput {
        id: Int!
        cedula: Int!
        username: String!
        password: String!
        name: String!
        last_name: String!
        cargo: String!
        email: String!
        adress: String!
        phoneNumber: Int!
    }
    
    type Mutation {
        signUpUser(userInput :SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
        updateUserAdmin(user: UserInput): UserAdmin!
        deleteUserAdminById(userId: Int!): UserAdmin
        
    }

    type Query {
        userDetailById(userId: Int!): UserDetail!
    }

`;
module.exports = userAdminTypeDefs;
    