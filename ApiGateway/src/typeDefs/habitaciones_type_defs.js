const { gql } = require('apollo-server');

const habitacionesTypeDefs = gql `
    type Habitacion {
        id: Int
        numeroHabitacion: String!
        cantidadCamas: String!
        tipoCama: String!
        acomodacion: String!
        valorporNoche: Int!
    }

    input HabitacionInput {
        id: Int!
        numeroHabitacion: String!
        cantidadCamas: String!
        tipoCama: String!
        acomodacion: String!
        valorporNoche: Int!
    }

    type Query {
        habitacionById(habitacionId: Int!): Habitacion
    }

    type Mutation{
        createHabitacion(habitacion: HabitacionInput!): Habitacion
        updateHabitacion(habitacion: HabitacionInput!): Habitacion
        deleteHabitacionById(id: Int!): Habitacion
    }

`;

module.exports = habitacionesTypeDefs;