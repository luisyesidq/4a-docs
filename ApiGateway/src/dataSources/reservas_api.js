const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');//trae la configuracion de server
class reservaAPI extends RESTDataSource {
    constructor() {
    super();
    this.baseURL = serverConfig.reservas_api_url;//variable de la url que se definio en server.js
    }
    async createReserva(reserva) {//resibe parametro habitacion
        reserva = new Object(JSON.parse(JSON.stringify(reserva)));
        return await this.post(`/reservas/api/ms3/save`, reserva);//crea reserva
    }
    async getReservas(reservas) {
        return await this.get(`/reservas/api/ms3/all`,reservas);//trae todos los reservas
    }
    async getReserva(reservaId) {
        return await this.get(`/reservas/api/ms3/find/${reservaId}/`);//busca una reserva
    }
    async putReserva(reserva){
        reserva = new Object(JSON.parse(JSON.stringify(reserva)));//actualiza 
        return await this.post(`/reservas/api/ms3/save`, reserva)
    }
    async deleteReserva(reservaId) {
        return await this.delete(`/reservas/api/ms3/delete/${reservaId}/`);//elimina una reserva 
    }
    
}
module.exports = reservaAPI;