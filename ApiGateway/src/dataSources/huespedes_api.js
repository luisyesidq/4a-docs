const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');//trae la configuracion de server
class huespedesAPI extends RESTDataSource {
    constructor() {
    super();
    this.baseURL = serverConfig.huespedes_api_url;//variable de la url que se definio en server.js
    }
    async createHuesped(huesped) {//resibe parametro habitacion
        huesped = new Object(JSON.parse(JSON.stringify(huesped)));
        return await this.post(`/huesped/api/ms2/save`, huesped);//crea huesped
    }
    async getHuespedes(huespedes) {
        return await this.get(`/huesped/api/ms2/all`,huespedes);//trae todos los huespedes
    }
    async getHuesped(huespedId) {
        return await this.get(`/huesped/api/ms2/find/${huespedId}/`);//busca un huesped
    }
    async putHuesped(huesped){
        huesped = new Object(JSON.parse(JSON.stringify(huesped)));//actualiza 
        return await this.post(`/huesped/api/ms2/save`, huesped)
    }
    async deleteHuesped(huespedId) {
        return await this.delete(`/huesped/api/ms2/delete/${huespedId}/`);//elimina un huesped
    }
    
}
module.exports = huespedesAPI;