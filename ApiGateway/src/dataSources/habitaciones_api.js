const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');//trae la configuracion de server
class habitacionesAPI extends RESTDataSource {
    constructor() {
    super();
    this.baseURL = serverConfig.habitaciones_api_url;//variable de la url que se definio en server.js
    }
    async createHabitacion(habitaciones) {//resibe parametro habitacion
        habitaciones = new Object(JSON.parse(JSON.stringify(habitaciones)));
        return await this.post(`/habitaciones/`, habitaciones);
    }
    async getHabitaciones(habitaciones) {
        return await this.get(`/habitaciones/`,habitaciones);
    }
    async getHabitacion(habitacionId) {
        return await this.get(`/habitacion/${habitacionId}/`);
    }
    async putHabitacion(habitacion){
        habitacion = new Object(JSON.parse(JSON.stringify(habitacion)));//pendiente preguntar si esta bien 
        return await this.put(`/habitacion/`, habitacion)
    }
    async deleteHabitacion(habitacionId) {
        return await this.delete(`/habitacion/${habitacionId}/`);
    }
}
module.exports = habitacionesAPI;