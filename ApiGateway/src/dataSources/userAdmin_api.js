const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');//trae la configuracion de server
class userAdminAPI extends RESTDataSource {
    constructor() {
    super();
    this.baseURL = serverConfig.userAdmin_api_url;//variable de la url que se definio en server.js
    }
    async createUser(user) {//resibe parametro user
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.post(`/user/`, user);
    }
    async getUser(userId) {
        return await this.get(`/user/${userId}/`);
    }
    async putUser(user) {//resibe parametro user
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.put(`/user/`, user);
    }
    
    async deleteUser(userId){
        return await this.delete(`/user/${userId}/`);
    }
    async userRequest(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post(`/login/`, credentials);
    }
    async refreshToken(token) {
        token = new Object(JSON.parse(JSON.stringify({ refresh: token })));
        return await this.post(`/refresh/`, token);
    }
}
module.exports = userAdminAPI;