const {ApolloServer} = require('apollo-server');

const typeDefs          = require('./typeDefs');
const resolvers         = require('./resolvers');
const HabitacionesAPI   = require('./dataSources/habitaciones_api');
const HuespedesAPI      = require('./dataSources/huespedes_api');
const ReservaAPI        = require('./dataSources/reservas_api');
const UserAdminAPI      = require('./dataSources/userAdmin_api');
const authentication    = require('./utils/authentication');

const server = new ApolloServer({
    context: authentication,
    typeDefs,
    resolvers,
    dataSources: () => ({
        habitacionesAPI: new HabitacionesAPI(),
        huespedesAPI: new HuespedesAPI(),
        reservaAPI: new ReservaAPI(),
        userAdminAPI: new UserAdminAPI(),
    }),
        introspection: true,
        playground: true
});
server.listen(process.env.PORT || 4000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});