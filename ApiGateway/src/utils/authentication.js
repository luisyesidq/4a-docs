const { ApolloError } = require('apollo-server');
const serverConfig = require('../server');//donde estan las url
const fetch = require('node-fetch');//funciones de recuperacion de datos
const authentication = async ({ req }) => {
const token = req.headers.authorization || '';//recibe el token de la peticion variable (req) o vasio  (|| '')
    if (token == '')
        return { userIdToken: null }//retorna en formato json el token id nulo
    else {
        try {
            let requestOptions = {//se define la variable requestOptions
                method: 'POST', headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ token }), redirect: 'follow'//comone las variables de la peticion y convierte el objeto a 
            };                                                       // un cadena txt string y hace una redireccon de la peticion http
        let response = await fetch(//se crea una variable response 
            `${serverConfig.userAdmin_api_url}/verifyToken/`,
            requestOptions)
        if (response.status != 200) {//si no responde 200 hubo un fallo 
            console.log(response)
            throw new ApolloError(`SESION INACTIVA - ${401}` + response.status, 401)
        }
        return { userIdToken: (await response.json()).UserId };//si la respuesta es 200
        }
        catch (error) {
            throw new ApolloError(`TOKEN ERROR: ${500}: ${error}`, 500);//lanza un erroe codigo 500
        }
    }
}
module.exports = authentication;//se exporta