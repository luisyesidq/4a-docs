const huespedesResolver = {
    Query: {
        huespedById: (_, { huespedesId }, { dataSources, userIdToken }) => {
            if (huespedesId !="")
                return dataSources.huespedesAPI.getHuesped(huespedesId)
            else
                return null
        
        },
    },
    Mutation: {
        createHuesped: async(_, { huesped }, { dataSources }) => {
            const creahuesped = {
                id                  : huesped.id,
                numeroidentificacion: huesped.numeroidentificacion,
                nombres             : huesped.nombres,
                apellidos           : huesped.apellidos,
                paisResidencia      : huesped.paisResidencia,
                direccion           : huesped.direccion,
                telefono            : huesped.telefono,
                correoElectronico   : huesped.correoElectronico,
                
            }
            return await dataSources.huespedesAPI.createHuesped(creahuesped);
        },
        
        updateHuesped: async(_, { huesped }, { dataSources }) => {
            const updahuesped = {
                id                  : huesped.id,
                numeroidentificacion: huesped.numeroidentificacion,
                nombres             : huesped.nombres,
                apellidos           : huesped.apellidos,
                paisResidencia      : huesped.paisResidencia,
                direccion           : huesped.direccion,
                telefono            : huesped.telefono,
                correoElectronico   : huesped.correoElectronico,
                
            }
            return await dataSources.huespedesAPI.putHuesped(updahuesped);
        },
        deleteHuespedById: async(_, { id }, { dataSources }) => {
            return await dataSources.huespedesAPI.deleteHuesped(id);
        }
    }
};
module.exports = huespedesResolver;