const habitacionResolver = {
    Query: {
        habitacionById: (_, { habitacionId }, { dataSources, userIdToken }) => {
            if (habitacionId !="")
                return dataSources.habitacionesAPI.getHabitacion(habitacionId)
            else
                return null
        
        },
    },
    Mutation: {
        createHabitacion: async(_, { habitacion }, { dataSources }) => {
            const creaHabitacion = {
                id                : habitacion.id,
                numeroHabitacion  : habitacion.numeroHabitacion,
                cantidadCamas     : habitacion.cantidadCamas,
                tipoCama          : habitacion.tipoCama,
                acomodacion       : habitacion.acomodacion,
                valorporNoche     : habitacion.valorporNoche,
                
            }
            return await dataSources.habitacionesAPI.createHabitacion(creaHabitacion);
        },
        
        updateHabitacion: async(_, { habitacion }, { dataSources }) => {
            const updaHabitacion = {
                id                : habitacion.id,
                numeroHabitacion  : habitacion.numeroHabitacion,
                cantidadCamas     : habitacion.cantidadCamas,
                tipoCama          : habitacion.tipoCama,
                acomodacion       : habitacion.acomodacion,
                valorporNoche     : habitacion.valorporNoche,
                
            }
            return await dataSources.habitacionesAPI.putHabitacion(updaHabitacion);
        },
        deleteHabitacionById: async(_, { id }, { dataSources }) => {
            return await dataSources.habitacionesAPI.deleteHabitacion(id);
        }
    }
};
module.exports = habitacionResolver;