const usersResolver = {
    Query: {
        userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
            if (userId == userIdToken)
                return dataSources.userAdminAPI.getUser(userId)
            else
                return null
        
        },
        
    },
    Mutation: {
        signUpUser: async(_, { userInput }, { dataSources }) => {
            const creauser = {
                cedula      : userInput.cedula,
                username    : userInput.username,
                password    : userInput.password,
                name        : userInput.name,
                last_name   : userInput.last_name,
                cargo       : userInput.cargo,
                email       : userInput.email,
                adress      : userInput.adress,
                phoneNumber : userInput.phoneNumber,
            }
            return await dataSources.userAdminAPI.createUser(creauser);
        },
        updateUserAdmin: async(_, { user }, { dataSources }) => {
            const updateUserAdmin = {
                id          : user.id,
                cedula      : user.cedula,
                username    : user.username,
                password    : user.password,
                name        : user.name,
                last_name   : user.last_name,
                cargo       : user.cargo,
                email       : user.email,
                adress      : user.adress,
                phoneNumber : user.phoneNumber,
            }
            return await dataSources.userAdminAPI.putUser(updateUserAdmin);
        },
        deleteUserAdminById: async(_, { userId }, { dataSources }) => {
            return await dataSources.userAdminAPI.deleteUser(userId);
      },
        
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.userAdminAPI.userRequest(credentials),
        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.userAdminAPI.refreshToken(refresh),

    }
};
module.exports = usersResolver;