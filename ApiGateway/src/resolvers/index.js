const habitacionResolver    = require('./habitaciones_resolver');
const huespedesResolver     = require('./huespedes_resolver');
const reservasResolver      = require('./reservas_resolver');
const usersResolver         = require('./userAdmin_resolver');

const lodash = require('lodash');

const resolvers = lodash.merge(habitacionResolver,huespedesResolver,reservasResolver,usersResolver);

module.exports =resolvers;