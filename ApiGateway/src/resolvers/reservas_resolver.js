const reservasResolver = {
    Query: {
        reservaById: (_, { reservaId }, { dataSources, userIdToken }) => {//recibe parametros  userId, dataSources, userIdToken
            if (reservaId !="")
                return dataSources.reservaAPI.getReserva(reservaId)
            else
                return null
        },
    },
    Mutation: {
        createReserva: async(_, { reserva }, { dataSources }) => {
            const creaReserva = {
                id                          : reserva.id,
                numeroidentificacion_huesped: reserva.numeroidentificacion_huesped,
                numeroHabitacion            : reserva.numeroHabitacion,
                fechaEntrada                : reserva.fechaEntrada,
                fechaSalida                 : reserva.fechaSalida,
                estado                      : reserva.estado,
            }
            await dataSources.reservaAPI.createReserva(creaReserva);
   
        },
        updateReserva: async(_, { reserva }, { dataSources }) => {
            const updaReserva = {
                id                          : reserva.id,
                numeroidentificacion_huesped: reserva.numeroidentificacion_huesped,
                numeroHabitacion            : reserva.numeroHabitacion,
                fechaEntrada                : reserva.fechaEntrada,
                fechaSalida                 : reserva.fechaSalida,
                estado                      : reserva.estado,
            }
            await dataSources.reservaAPI.putReserva(updaReserva);
    
        },
        deleteReservaById: async(_, { id }, { dataSources }) => {
            await dataSources.reservaAPI.deleteReserva(id);
    
        },
    }
};
module.exports = reservasResolver;