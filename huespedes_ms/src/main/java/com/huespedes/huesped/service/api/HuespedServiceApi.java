package com.huespedes.huesped.service.api;

import com.huespedes.huesped.commons.GenericServiceApi;
import com.huespedes.huesped.model.Huesped;

public interface HuespedServiceApi extends GenericServiceApi<Huesped, Long> {
}
