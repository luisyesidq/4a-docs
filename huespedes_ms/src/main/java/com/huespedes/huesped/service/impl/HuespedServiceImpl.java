package com.huespedes.huesped.service.impl;

import com.huespedes.huesped.commons.GenericServiceImpl;
import com.huespedes.huesped.dao.api.HuespedRepositorio;
import com.huespedes.huesped.model.Huesped;
import com.huespedes.huesped.service.api.HuespedServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class HuespedServiceImpl extends GenericServiceImpl<Huesped, Long> implements HuespedServiceApi {

    @Autowired
    private HuespedRepositorio huespedRepository;

    @Override
    public CrudRepository<Huesped, Long> getDao() {
        return huespedRepository;
    }
}
