package com.huespedes.huesped.dao.api;

import com.huespedes.huesped.model.Huesped;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HuespedRepositorio  extends MongoRepository<Huesped, Long> {

}
