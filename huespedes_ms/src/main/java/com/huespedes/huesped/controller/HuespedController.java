package com.huespedes.huesped.controller;

import com.huespedes.huesped.model.Huesped;
import com.huespedes.huesped.service.api.HuespedServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/huesped/api/ms2")
@CrossOrigin("*")
public class HuespedController {

    @Autowired
    private HuespedServiceApi huespedServiceApi;


    @GetMapping(value = "/all")
    public List<Huesped> getAll()
    {
        return huespedServiceApi.getAll();
    }

    @GetMapping(value = "/find/{id}")
    public Huesped find(@PathVariable Long id)
    {
        return huespedServiceApi.get(id);
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Huesped> save(@RequestBody Huesped huesped)
    {
        Huesped obj = huespedServiceApi.save(huesped);
        return new ResponseEntity<Huesped>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Huesped> delete(@PathVariable Long id)
    {
        Huesped huesped = huespedServiceApi.get(id);
        if(huesped!=null)
        {
            huespedServiceApi.delete(id);
        }
        else
        {
            return new ResponseEntity<Huesped>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Huesped>(HttpStatus.OK);
    }
}
