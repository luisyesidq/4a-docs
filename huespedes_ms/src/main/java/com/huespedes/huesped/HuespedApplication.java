package com.huespedes.huesped;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HuespedApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuespedApplication.class, args);
	}

}
