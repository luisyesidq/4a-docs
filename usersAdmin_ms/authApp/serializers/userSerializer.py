from rest_framework import serializers
from authApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'cedula','username', 'password', 'name', 'last_name', 'cargo', 'email', 'adress','phoneNumber']