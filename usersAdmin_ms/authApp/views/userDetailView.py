# se importan para poder generar las respuestas http
from rest_framework.response import Response
# se importa para poder consumir las vistas de ayuda 
from rest_framework.views import APIView
# se importa para poder modificar las vistas de ayuda
from rest_framework.decorators import api_view
#se importa para poder usar el modelo creado
from authApp.models import User
# se importa para poder manipular el modelo con el serializador creado
from authApp.serializers import UserSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','PUT','DELETE'])
def UserDetailView(request,pk=None):
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return Response("no se encontro id")

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        user = User.objects.filter(id = pk).first()
        # se serializa los datos para la manipulacion
        user_serializer = UserSerializer(user)
        # se envia respuesta al solicitante
        return Response(user_serializer.data)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
            
        return Response(serializer.errors)
        

    # se filtran los mensajes enviados por PUT
    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        user = User.objects.filter(id = pk).first()
        # se borra el dato de la base de datos
        user.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")